# Gothic 2 Online - ORM
This repository contains a ORM(object-relational mapper) based on "Active record pattern". Designed for usage on Gothic 2 Online platform and fully written in Squirrel.

# Attention
For this framework to work properly(or any at all) you need atleast one of the available SQL Database engine implementing modules:
  - [SQLite]
  - [MySQL]
  - Async MySQL (future plans...)

# Future plans
Current state of project is still far from ideal.
Below are my future plans for the project, which I aim to get done.
  - Asynchronous MySQL implementation
  - Protection against SQL Injection
  - Mapping columns to class fields
  - Code and queries optimization
  - Caching of queried records

# Usage example:
In order to start using working with ORM, you need to setup your connection with the database.

## # Connection
The connection is made differently between different engines. However, we always need to specify the connection id in the constructor.

#### SQLite
```js
local connection = ORM.SQLite("game")
connection.open("filename")
```

#### MySQL
```js
local connection = ORM.MySQL("game")
connection.open("database", "login", "password", "hostname")
```

*'game' is the id of the connection, which we'll use later...*

## # SQL execution on database connection
To execute sql on our database connection, we just need to use one of the following methods, on our connection object:
```js
//method 'exec' doesn't return anything
connection.exec("INSERT INTO table (column) VALUES (value)")

//while method 'findAll' returns an array of selected records
local results = connection.findAll("SELECT * FROM table")
```

## # Models (i.e. Tables representations in Squirrel)
In order to use the full potential of the ORM, we need to define a class which will represent the table from our database.
So, we will create a class named `DbPlayer` which will be representing the following table:
```sql
CREATE TABLE players (
  id        INT(11)     NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username  VARCHAR(50) NOT NULL
)
```
Our `DbPlayer` class needs to extend `ORM.Model` class to work, and have the following [attributes] set:
  - 'database' - the database id which will determine what connection to use
  - 'table' - the table from we are going to query
```js
class DbPlayer extends ORM.Model
</ database = "game", table = "players" />
{
  //...
}
```

## # Creating new records
To create and insert new record to table. We just need to instantiate a class, and call on newly created object the following methods:
```js
local player = DbPlayer()
player.set("username", "Diego") //set new value to 'username' column
player.insert() //Insert new record to table
```

## # Selecting records
Selecting one record by id:
```js
local player = DbPlayer.findById(0) //Where '0' is the id
```
Selecting first record with given condition:
```js
local player = DbPlayer.first("username = ?", "Diego")
```
Selecting all records from table:
```js
local players = DbPlayer.findAll()

foreach(player in players)
  print("id: " + player.getId() + ", username: " + player.get("username"))
```
Selecting all records with given condition:
```js
local players = DbPlayer.where("username = ? OR username = ?", "Lares", "Diego")

foreach(player in players)
  print("id: " + player.getId() + ", username: " + player.get("username"))
```
*In the last case, the method 'where' returns us, a Lazy List which query the records only when is going to be read. The list can be manipulated to further adjust the records that are going to be selected, for example: we can set offset, limit, and force instant query the results.*
```js
local players = DbPlayer.where("username = ? OR username = ?", "Lares", "Diego").offset(100).limit(5).load()
```
## # Updating records
```js
local player = DbPlayer.findById(0)
player.set("username", "Lares")
player.save()
```
*If the record doesn't exist in the database, it will be automatically created*

## # Counting records
The record counting can be done using both: the model(our DbPlayer class) or the connection.

#### Model
```js
//With specific criteria
local count = DbPlayer.count("username = ?", "Diego")
//Or all of the records
local count = DbPlayer.count()
```

#### Connection
```js
//With specific criteria
local count = connection.count("players", "username = ?", "Diego")
//Or all of the records
local count = connection.count("players")
```

## # Refreshing records
When our record is modified but not saved, and we need to restore the old data, we just need to execute the 'refresh' method:
```js
local player = DbPlayer.findById(0)
player.set("username", "some unwanted name")
//...
player.refresh()
```

## # Deleting records
```js
local player = DbPlayer.findById(0)
player.remove()
```

# Foreign keys
**Foreign keys are still an incomplete feature, and are missing some of the functionality** <br />
#
To use the power of foreign keys in ORM, we need to add one more attribute to our table representing classes:
  - 'parents' - An array containing the columns and the classes to which are linked

To not get you bored by my speak, let's jump to the examples.
Let's say we have the following tables in our database:
```sql
CREATE TABLE authors (
  id          INT(11)     NOT NULL PRIMARY KEY AUTO_INCREMENT,
  first_name  VARCHAR(50) NOT NULL,
  last_name  VARCHAR(50) NOT NULL
)

CREATE TABLE books (
  id        INT(11)     NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name      VARCHAR(50) NOT NULL,
  author_id INT(11)     NOT NULL
)
```
And the following tables representation in squirrel:
```js
class DbAuthor extends ORM.Model
</ database = "default", table = "authors" />
{
  //...
}

class DbBook extends ORM.Model
</ 
    database = "default", table = "books",
    parents = [{ model = DbAuthor, key = "author_id" }]
/>
{
  //...
}
```
Now we want to create a book record which will point to the author. And to do that we can manually set the value of 'author_id', or do this:
```js
local author = DbAuthor()
author.set("first_name", "William")
author.set("last_name", "Shakespeare")
author.insert() //Creating of the parent record

local book = DbBook()
book.set("name", "Hamlet")
book.setParent(author) //Setting previously created record as a parent (This method sets the value of 'author_id' to id of author)
book.insert()
```
Retrieving the author of the book(the parent from the children):
```js
local book = DbBook.findById(0)
local author = book.parent(DbAuthor)// <------
```

Retrieving the books of our author(the children of our parent): 
```js
local author = DbAuthor.findById(0)
local books = author.getAll(DbBook)// <------

foreach(book in books)
  print("book name: " + book.get("name"))
```

# License
This project is licensed under the MIT License - see the [LICENSE] file for details.

[SQLite]: <https://gothic-online.com.pl/forum/thread-2451.html>
[MySQL]: <https://gothic-online.com.pl/forum/thread-25.html>
[attributes]: <http://www.squirrel-lang.org/squirreldoc/reference/language/classes.html?highlight=attribute#class-attributes>
[LICENSE]: <https://gitlab.com/carelioncanem/g2o-orm/-/blob/master/LICENSE>

