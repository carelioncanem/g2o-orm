ORM <- {}

class ORM.Statement
{
    function SELECT(list)
    {
        local query = ::format("SELECT * FROM %s", list._model.getTableName())

        if(list._query.len() > 0)
            query += " WHERE " + list._query

        if(list._limit != -1)
            query += " LIMIT " + list._limit

        if(list._offset != -1)
            query += " OFFSET " + list._offset

        return query
    }

    function INSERT(model)
    {
        local modified = model.getModifiedAttributes()

        if(modified.len() > 0)
        {
            local columns = "", values = ""

            foreach(idx,value in modified)
            {
                if(type(value) == "string")
                    value = "\"" + value + "\""

                columns += columns.len() > 0 ? ("," + idx) : idx
                values += values.len() > 0 ? ("," + value) : value
            }

            return ::format("INSERT INTO %s (%s) VALUES (%s)", model.getTableName(), columns, values)
        }
        else
            return ::format("INSERT INTO %s DEFAULT VALUES", model.getTableName())
    }

    function UPDATE(model)
    {
        local modified = model.getModifiedAttributes()
        local values = ""

        foreach(idx,values in modified)
        {
            if(type(value) == "string")
                    value = "\"" + value + "\""

            values += (values.len() > 0 ? ("," + idx) : idx) + "=" + value
        }

        return ::format("UPDATE %s SET %s WHERE %s=\"%s\"", model.getTableName(), values, model.getIdName(), model.getId())
    }

    function DELETE(model)
    {
        return ::format("DELETE FROM %s WHERE %s=\"%s\"", model.getTableName(), model.getIdName(), model.getId())
    }

    function COUNT(table, subQuery = null)
    {
        local query = "SELECT COUNT(*) FROM %s"

        if(subQuery != null && subQuery.len() > 0)
            query += " WHERE " + subQuery

        return ::format(query, table)
    }

    function format(query, params)
    {
        if(query.len() == 0 || params.len() == 0)
            return query

        local pos = -1, current = 0
        while((pos = query.find("?", pos + 1)) != null && current < params.len())
            query = query.slice(0, pos) + (type(params[current]) == "string" ? ("\"" + params[current++] + "\"") : params[current++]) + query.slice(pos + 1, query.len())

        return query
    }
}
