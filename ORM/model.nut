class ORM.Model
{
    _attributes = null
    _dirty = null
    _frozen = null

    constructor()
    {
        _attributes = {}
        _dirty = []
        _frozen = false
    }

    static function _getModelAttributes() { return type(this) == "class" ? getattributes(null) : getclass().getattributes(null) }

    static function getIdName()
    {
        local attributes = _getModelAttributes()
        return "id" in attributes ? attributes["id"] : "id"
    }
    static function getTableName()
    {
        local attributes = _getModelAttributes()

        if(!("table" in attributes))
            throw("Your ORM.Model is missing required attribute 'table'")

        return attributes["table"]
    }
    static function getDatabaseName()
    {
        local attributes = _getModelAttributes()

        if(!("database" in attributes))
            throw("Your ORM.Model is missing required attribute 'database'")

        return attributes["database"]
    }

    static function getDatabase()
    {
        local dbName = getDatabaseName()
        local database = ORM.Engine.getDatabase(dbName)

        if(database == null)
            throw("Connection with id '" + dbName + "' doesn't exists")

        if(!database.hasConnection())
            throw("'" + dbName + "' has not active connection")

        return database
    }

    static function count(...)
    {
        local database = getDatabase()

        vargv.insert(0, database)
        vargv.insert(1, getTableName())

        return database.count.acall(vargv)
    }

    static function where(query, ...)
    {
        return ORM.List(type(this) == "class" ? this : getclass(), ORM.Statement.format(query, vargv))
    }
    static function first(query, ...)
    {
        local list = ORM.List(type(this) == "class" ? this : getclass(), ORM.Statement.format(query, vargv))
        return list.len() > 0 ? list[0] : null
    }
    static function findAll()
    {
        return where("")
    }
    static function findById(id)
    {
        return first(getIdName() + " = ?", id)
    }

    function isNew() { return !(getIdName() in _attributes) }
    function isModified() { return _dirty.len() > 0 }
    function getAttributes() { return _attributes }
    function getModifiedAttributes()
    {
        local elements = {}

        foreach(key in _dirty)
            elements[key] <- _attributes[key]

        return elements
    }

    function get(key)  { return key in _attributes ? _attributes[key] : null }
    function set(key, value)
    {
        if(key in _attributes && _attributes[key] == value)
            return
        
        if(!(key in _dirty))
            _dirty.push(key)

        _attributes[key] <- value
    }
    function getId() { return get(getIdName()) }
    function setId(value) { return set(getIdName(), value) }

    function getAll(model)
    {
         if(parent.isNew())
            throw("Parent model must be saved before retrieving his children")

        local _base = model.getbase()
        while(_base && _base != ORM.Model)
            _base = _base.getbase()

        if(_base != ORM.Model)
            throw("Child must extends ORM.Model class")

        local attributes = model._getModelAttributes()

        if(!("parents" in attributes))
            throw("Child ORM.Model is missing required attribute 'parents'")

        foreach(entry in attributes["parents"])
        {
            if(getclass() == entry.model)
                return model.where(entry.key + " = ?", getId())
        }

        return null
    }

    function parent(model)
    {
        local _base = model.getbase()
        while(_base && _base != ORM.Model)
            _base = _base.getbase()

        if(_base != ORM.Model)
            throw("Parent must extends ORM.Model class")

        local attributes = _getModelAttributes()

        if(!("parents" in attributes))
            throw("Your ORM.Model is missing required attribute 'parents'")

        foreach(entry in attributes["parents"])
        {
            if(model == entry.model)
            {
                if(!(entry.key in _attributes))
                    throw("Missing foreign key value at column '" + entry.key + "'")

                return model.findById(get(entry.key))
            }
        }

        return null
    }
    function setParent(parent)
    {
        if(!(parent instanceof ORM.Model))
            throw("Parent must be an object of class extending ORM.Model")

        if(parent.isNew())
            throw("Parent model must be saved before setting him as one")

        local attributes = _getModelAttributes()

        if(!("parents" in attributes))
            throw("Your ORM.Model is missing required attribute 'parents'")

        foreach(entry in attributes["parents"])
        {
            if(parent instanceof entry.model)
            {
                set(entry.key, parent.getId())
                return
            }
        }
    }

    function insert()
    {
        if(getDatabase().insert(this))
            _dirty.clear()
    }
    function save()
    {
        if(!isNew())
        {
            if(getDatabase().update(this))
                _dirty.clear()
        }
        else insert()
    }
    function remove()
    {
        getDatabase().remove(this)
        _frozen = true
    }
    function reset()
    {
        _attributes.clear()
        _dirty.clear()
    }
    function refresh()
    {
        if(isNew())
            throw("Cannot refresh self because record with this Id probably doesn't exist yet in the database")

        local fresh = findById(getId())
        reset()

        foreach(key,value in fresh._attributes)
            _attributes[key] <- value
    }
}