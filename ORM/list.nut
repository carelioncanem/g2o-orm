class ORM.List
{
    _data = null
    _model = null
    _query = null

    _limit = null
    _offset = null
    _loaded = null

    constructor(model, query)
    {
        _data = []
        _model = model
        _query = query

        _limit = -1
        _offset = -1
        _loaded = false
    }

    function limit(number)
    {
        if(number < 0)
            throw("limit cannot be negative")

        _limit = number
        return this
    }

    function offset(number)
    {
        if(number < 0)
            throw("offset cannot be negative")

        _offset = number
        return this
    }

    function load()
    {
        if(_loaded)
            return

        _model.getDatabase().select(this)
        _loaded = true

        return this
    }

    /******************BASIC********************/
    function append(val)                { return _data.append(val)          }
    function push(val)                  { return _data.push(val)            }
    function extend(array)              { return _data.extend(array)        }
    function pop()                      { return _data.pop()                }
    function top()                      { return _data.pop()                }
    function insert(idx, val)           { return _data.insert(idx, val)     }
    function remove(idx)                { return _data.remove(idx)          }
    function resize(size, fill = null)  { return _data.resize(size, fill)   }
    function clear()                    { return _data.clear()              }

    function len()                      { load(); return _data.len()                }
    function find(value)                { load(); return _data.find(value)          }
    function sort(compare_func = null)  { load(); return _data.sort(compare_func)   }
    function reverse()                  { load(); return _data.reverse()            }
    function slice(start, end = null)   { load(); return _data.slice(start, end)    }
    function map(func)                  { load(); return _data.map(func)            }
    function apply(func)                { load(); return _data.apply(func)          }
    function reduce(func)               { load(); return _data.reduce(func)         }
    function filter(func)               { load(); return _data.filter(func)         }
    
    function _nexti(previdx)
    {
        load()
        local idx = previdx == null ? 0 : previdx + 1
        return idx < len() ? idx : null
    }

    function _get(idx)
    {
        load()
        if(idx in _data)
            return _data[idx]

       throw null
    }
}