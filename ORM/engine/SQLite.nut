local sqlite_data = null

addEventHandler("sqlite3_onSelect", function(idx, results)
{
    switch(idx)
    {
        case "insert":
            sqlite_data._attributes[sqlite_data.getIdName()] <- results["id"]
            break

        case "select":
            local instance = sqlite_data._model.instance()
            instance.constructor()

            foreach(idx,value in results)
                instance._attributes[idx] <- value

            sqlite_data.push(instance)
            break

        case "find":
            sqlite_data.push(results)
            break
    }
})

class ORM.SQLite extends ORM.Engine
{
    function open(database)
    {
        if(hasConnection())
            throw("'" + _id + "' is already connected to database")

        _handle = sqlite3_openDB(database)
    }

    function close()
    {
        if(hasConnection())
            sqlite3_closeDB(_handle)
    }

    function findAll(query, ...)
    {
        if(!hasConnection())
            throw("'" + _id + "' has not active connection")

        sqlite_data = []

        if(!sqlite3_execSelect(_handle, "find", ORM.Statement.format(query, vargv)))
            throw(sqlite3_errmsg(_handle))

        local result = sqlite_data
        sqlite_data = null

        return result
    }

    function exec(query, ...)
    {
        if(!hasConnection())
            throw("'" + _id + "' has not active connection")

        if(!sqlite3_exec(_handle, ORM.Statement.format(query, vargv)))
            throw(sqlite3_errmsg(_handle))
    }

    function select(list)
    {
        if(list._limit == -1 && list._offset != -1)
            throw("SQLite does not support 'offset' without 'limit'")

        sqlite_data = list
        if(!sqlite3_execSelect(_handle, "select", ORM.Statement.SELECT(list)))
            throw(sqlite3_errmsg(_handle))

        sqlite_data = null
    }

    function insert(model)
    {
        if(!sqlite3_exec(_handle, ORM.Statement.INSERT(model)))
            throw(sqlite3_errmsg(_handle))

        sqlite_data = model
        sqlite3_execSelect(_handle, "insert", "SELECT LAST_INSERT_ROWID() AS id")
        sqlite_data = null
    }

    function update(model)
    {
        if(!sqlite3_exec(_handle, ORM.Statement.UPDATE(model)))
            throw(sqlite3_errmsg(_handle))
    }

    function remove(model)
    {
        if(!sqlite3_exec(_handle, ORM.Statement.DELETE(model)))
            throw(sqlite3_errmsg(_handle))
    }
}