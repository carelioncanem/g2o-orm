class ORM.MySQL extends ORM.Engine
{
    function open(database, username, password, hostname, port = 3306)
    {
        if(hasConnection())
            throw("'" + _id + "' is already connected to database")

        _handle = mysql_connect(hostname, username, password, database, port)
    }

    function close()
    {
        if(hasConnection())
            mysql_close(_handle)
    }

    function findAll(query, ...)
    {
        if(!hasConnection())
            throw("'" + _id + "' has not active connection")

        local result = mysql_query(_handle, ORM.Statement.format(query, vargv))

        if(mysql_errno(_handle))
            throw(mysql_error(_handle))

        local results = []
        local row = mysql_fetch_assoc(result)

        if(row != null)
            results.push(row)

        return results
    }

    function exec(query, ...)
    {
        if(!hasConnection())
            throw("'" + _id + "' has not active connection")

        mysql_query(_handle, ORM.Statement.format(query, vargv))

        if(mysql_errno(_handle))
            throw(mysql_error(_handle))
    }

    function select(list)
    {
        local result = mysql_query(_handle, ORM.Statement.SELECT(list))

        if(mysql_errno(_handle))
            throw(mysql_error(_handle))

        local row = null
        while(row = mysql_fetch_assoc(result))
        {
            local instance = list._model.instance()
            instance.constructor()

            foreach(idx,value in row)
                instance._attributes[idx] <- value

            list.push(instance)
        }
    }

    function insert(model)
    {
        if(!mysql_query(_handle, ORM.Statement.INSERT(model)))
            throw(mysql_error(_handle))

        local result = mysql_query(_handle, "SELECT LAST_INSERT_ID()")

        if(mysql_errno(_handle))
            throw(mysql_error(_handle))

        local row = mysql_fetch_row(result)
        if(row != null)
           model._attributes[model.getIdName()] <- row[0]
    }

    function update(model)
    {
        if(!mysql_query(_handle, ORM.Statement.UPDATE(model)))
            throw(mysql_error(_handle))
    }

    function remove(model)
    {
        if(!mysql_query(_handle, ORM.Statement.DELETE(model)))
            throw(mysql_error(_handle))
    }
}
