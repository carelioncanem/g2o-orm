class ORM.Engine
{
    /****   STATIC  ****/
    static connections = {}
    static function getDatabase(idx) { return idx in connections ? connections[idx] : null }

    /****   LOCAL  ****/
    _id = null
    _handle = null

    constructor(idx)
    {
        _id = idx
        connections[idx] <- this
    }


    function firstCell(query, ...)
    {
        vargv.insert(0, this)
        vargv.insert(1, query)

        local results = findAll.acall(vargv)

        if(results.len() > 0)
        {
            foreach(idx,value in results[0])
                return value
        }
        else
            return null
    }

    function firstColumn(query, ...)
    {
        vargv.insert(0, this)
        vargv.insert(1, query)

        local results = findAll.acall(vargv)
        local result = []

        if(results.len() > 0)
        {
            local column = null

            foreach(entry in results)
            {
                if(column == null)
                {
                    foreach(idx,value in entry)
                    {
                        column = idx
                        break
                    }
                }

                result.push(entry[column])
            }
        }

        return result
    }

    //  count(table)
    //  count(table, query, ...)
    function count(...)
    {
        if(vargv.len() == 0)
            throw("Missing atleast one parameter, while using method 'count'. Check docs, for further details...")

        local table = vargv[0]
        local query = null

        if(vargv.len() > 1)
        {
            query = vargv[1]
            vargv.remove(0) //remove 'table'
            vargv.remove(0) //remove 'query'

            query = ORM.Statement.format(query, vargv)
        }

        return firstCell(ORM.Statement.COUNT(table, query))
    }
    
    function hasConnection() { return _handle != null }

    function findAll(query, ...) { throw("method 'query' is not overloaded") }
    function exec(query, ...) { throw("method 'exec' is not overloaded") }

    function select(list) { throw("method 'select' is not overloaded") }
    function insert(model) { throw("method 'insert' is not overloaded") }
    function update(model) { throw("method 'update' is not overloaded") }
    function remove(model) { throw("method 'remove' is not overloaded") }
}
